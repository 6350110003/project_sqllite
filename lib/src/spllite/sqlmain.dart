import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:project_sqllite/src/spllite/pages/list_profile.dart';


void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SQLite Demo',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: const ListProfile(title: 'Profile Lists'),
      debugShowCheckedModeBanner: false,
    );
  }
}



