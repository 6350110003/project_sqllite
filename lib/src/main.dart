import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_sqllite/src/cubit/app_cubit_logics.dart';
import 'package:project_sqllite/src/cubit/app_cubits.dart';
import 'package:project_sqllite/src/page/detail_page.dart';
import 'package:project_sqllite/src/page/home.dart';
import 'package:project_sqllite/src/services/data_sarvices.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: BlocProvider<AppCubits>(
        create:(context)=>AppCubits(
          data: DataServices(),
        ),
        child: AppCubitLogics(),
      )
    );
  }
}
