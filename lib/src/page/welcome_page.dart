import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_sqllite/src/cubit/app_cubits.dart';
import 'package:project_sqllite/src/page/responsive_button.dart';
import 'package:project_sqllite/src/widgets/app_buttons.dart';
import 'package:project_sqllite/src/widgets/app_large_text.dart';
import 'package:project_sqllite/src/widgets/app_text.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  List images=[
    'back.jpg',
    'back_2.jpg',
    'back_3.jpg'
  ];
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body:PageView.builder(
          scrollDirection: Axis.vertical,
          itemCount: images.length,
          itemBuilder: (_,index){
            return Container(
              width: double.maxFinite,
              height: double.maxFinite,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    'asset/images/'+images[index]
                  ),
                  fit: BoxFit.cover
                )
              ),
              child: Container(
                margin: const EdgeInsets.only(top:150,left: 2,right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(

                      children:
                      [
                        AppLargeText(text: 'Mood &   ',size:50,color: Colors.white,),
                        AppLargeText(text: ' Café',size:40,color:Colors.white,),
                        SizedBox(height: 10,),
                        Container(
                          margin: const EdgeInsets.only(left: 40),
                          width: 250,
                          child: AppText(
                            text: '   "  The only app that combines many cafesThe only app that combines many cafes  " ',
                            color: Colors.white60,
                            size: 14,
                          ),

                        ),
                        GestureDetector(
                          onTap: (){
                            BlocProvider.of<AppCubits>(context).getData();
                          },
                          child: Container(
                            margin: const EdgeInsets.only(top: 20),
                            width: 150,
                            height: 30,
                            child: Row(
                              children :[AppButtons( size: 80,
                                color:Colors.white60,
                                backgroundColor: Colors.black26,
                                borderColor: Colors.brown,
                                text: 'Joint Now',
                              ),
                              ]
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: List.generate(3, (indexDots){
                        return Container(
                          margin: const EdgeInsets.only(bottom: 2),
                          width: 8,
                          height: index==indexDots?25:8,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: index==indexDots?Colors.brown:Colors.brown.withOpacity(0.3)
                          ),
                        );
                      }),
                    )
                  ],
                ),
              ),

            );

      }),
    );
  }
}
