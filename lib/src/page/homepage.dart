import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_sqllite/src/cubit/app_cubit_states.dart';
import 'package:project_sqllite/src/cubit/app_cubits.dart';
import 'package:project_sqllite/src/page/detail_page.dart';
import 'package:project_sqllite/src/page/developer_user.dart';
import 'package:project_sqllite/src/page/search.dart';
import 'package:project_sqllite/src/page/user.dart';
import 'package:project_sqllite/src/page/welcome_page.dart';

import 'package:project_sqllite/src/spllite/pages/list_profile.dart';


import 'package:project_sqllite/src/spllite/pages/show_profile.dart';
import 'package:project_sqllite/src/spllite/sqlmain.dart';
import 'package:project_sqllite/src/widgets/app_buttons.dart';
import 'package:project_sqllite/src/widgets/app_large_text.dart';
import 'package:project_sqllite/src/widgets/app_text.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);



  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  var images ={
    "cake.jpg":"Cake",
    "drink.jpg":"Drink",
    "food.jpg":"Food",
    "icecreem.jpg":"Ice Creem",
  };


  
  @override
  Widget build(BuildContext context) {
    TabController _tabController = TabController(length: 3, vsync: this);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('Mood & Cafe'),
      ),
      body:
      BlocBuilder<AppCubits,CubitStates>(
        builder: (context,state){
         if(state is LoadedState){

           return Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: [
               //menu text
               Container(
                 padding: const EdgeInsets.only(top:3),
                 child: Row(
                   children: [
                   ],
                 ),
               ),
               SizedBox(height: 20,),
               //cafe text
               Container(
                 margin: const EdgeInsets.only(left: 20),
                 child: AppLargeText(text: 'Mood & Café',),
               ),
               SizedBox(height: 20,),
               //tabbar
               Container(
                 child: Align(
                   alignment: Alignment.centerLeft,
                   child: TabBar(
                     labelPadding: const EdgeInsets.only(left: 20,right: 20),
                     controller: _tabController,
                     labelColor: Colors.black,
                     unselectedLabelColor: Colors.grey,
                     isScrollable: true,
                     indicatorSize: TabBarIndicatorSize.label,
                     indicator: CircleTabIndicator(color: Colors.black54,radius: 4),
                     tabs: [
                       Tab(text: 'Please'),
                       Tab(text: 'Signatur'),
                       Tab(text: 'Detail'),
                     ],
                   ),
                 ),
               ),
               Container(
                 padding: const EdgeInsets.only(left: 20),
                 height: 300,
                 width: double.maxFinite,
                 child: TabBarView(
                   controller: _tabController,
                   children: [
                     ListView.builder(

                       itemCount:3,
                       scrollDirection: Axis.horizontal,
                       itemBuilder: (BuildContext context, int index) {
                         return
                           Container(

                             margin: const EdgeInsets.only(right: 15,top: 10),
                             width: 200,
                             height: 300,

                             decoration: BoxDecoration(

                                 borderRadius: BorderRadius.circular(20),
                                 color: Colors.brown,

                                 image: DecorationImage(

                                     image:AssetImage(
                                         'asset/images/'+images.keys.elementAt(index),

                                     ),

                                     fit: BoxFit.cover
                                 )
                             ),
                             child: Container(
                               margin: const EdgeInsets.only(top:230),
                               width: 50,
                               child: ListTile(

                                 leading: Icon(
                                   Icons.bakery_dining_outlined,color: Colors.white,size: 30,),
                                 onTap: () {
                                   //Navigator.pop(context);
                                   Navigator.of(context).push(
                                       MaterialPageRoute(
                                         builder: (context) => DetailPage(),
                                       )
                                   );
                                 },
                               ),
                             ),



                           );




                       },


                     ),
                     ListTile(
                       leading: Icon(
                         Icons.coffee,
                       ),
                       title: const Text('Whit Table'),
                       onTap: () {
                         //Navigator.pop(context);
                         Navigator.of(context).push(
                             MaterialPageRoute(
                                 builder: (context) => DetailPage(),
                             )

                         );
                       },
                     ),
                     Text('Content disclaimer Content on this website is provided for information purposes only. Information about a therapy, service, product or treatment does not in any way endorse or support such therapy, service, product or treatment and is not intended to replace advice from your doctor or other registered health professional. The information and materials contained on this website are not intended to constitute a comprehensive guide concerning all aspects of the therapy, product or treatment described on the website. All users are urged to always seek advice from a registered health care professional for diagnosis and answers to their medical questions and to ascertain whether the particular therapy, service, product or treatment described on the website is suitable in their circumstances. The State of Victoria and the Department of Health shall not bear any liability for reliance by any user on the materials contained on this website.')

                   ],
                 ),
               ),
               SizedBox(height: 30,),
               Container(
                 margin: const EdgeInsets.only(left: 20,right: 20),
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: [
                     AppLargeText(text: 'Explore more',size: 22,),
                     AppText(text: 'Sea all',color: Colors.black38,)
                   ],
                 ),
               ),
               SizedBox(height: 10,),
               Container(
                 height: 100,
                 width: double.maxFinite,
                 margin: const EdgeInsets.only(left: 20),
                 child: ListView.builder(
                     itemCount: 4,
                     scrollDirection: Axis.horizontal,
                     itemBuilder: (_, index){
                       return Container(
                         margin: const EdgeInsets.only(right: 35),
                         child: Column(
                           children: [
                             Container(
                               //margin: const EdgeInsets.only(right: 50),
                               width: 70,
                               height: 70,

                               decoration: BoxDecoration(
                                   borderRadius: BorderRadius.circular(20),
                                   color: Colors.black,
                                   image: DecorationImage(
                                       image:AssetImage('asset/images/'+images.keys.elementAt(index)),
                                       fit: BoxFit.cover)),
                             ),


                             Container(
                               child: AppText(
                                 text: images.values.elementAt(index),
                                 color: Colors.black54,
                               ),
                             )


                           ],
                         ),
                       );
                     }),
               ),




             ],
           );

         }else{
           return Container();
         }
        },

      ),
      drawer:Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [

            const UserAccountsDrawerHeader( // <-- SEE HERE
              decoration: BoxDecoration(color: Colors.black),
              accountName: Text(
                "Meow Meow ww",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
              accountEmail: Text(
                "cat_meow99@psu.ac.th",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              //currentAccountPicture: FlutterLogo(),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage("asset/images/cat.jpg",),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
              ),
              title: const Text('Detail'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            Divider(
              thickness: 0.8,
            ),
            ListTile(
              leading: Icon(
                Icons.people,
              ),
              title: const Text('System Developer',),
              onTap: () {
                //Navigator.pop(context);
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ProfileDeveloper(title: ''),
                    )

                );
              },
            ),
            Divider(
              thickness: 0.8,
            ),
            ListTile(
              leading: Icon(
                Icons.chair,
              ),
              title: const Text('Reservation'),
              onTap: () {
                //Navigator.pop(context);
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ListProfile(title: 'Reservation',),
                    )

                );
              },
            ),
            Divider(
              thickness: 0.8,
            ),
            ListTile(
              leading: Icon(
                Icons.search,
              ),
              title: const Text('Search'),
              onTap: () {
                //Navigator.pop(context);
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => Search(),
                    )

                );
              },
            ),
            Divider(
              thickness: 0.8,
            ),

          ],
        ),
      ),

    );
  }
}

class CircleTabIndicator extends Decoration{
  final Color color;
  double radius;
  CircleTabIndicator({required this.color,required this.radius});
  @override
  BoxPainter createBoxPainter([VoidCallback? onChanged]) {
    // TODO: implement createBoxPainter
    return _CirclePainter(color:color,radius:radius);
  }



  }
class _CirclePainter extends BoxPainter{
  final Color color;
  double radius;
  _CirclePainter({required this.color,required this.radius});

  @override
  void paint(Canvas canvas, Offset offset,
      ImageConfiguration configuration) {
      Paint _paint = Paint();
      _paint.color=color;
      _paint.isAntiAlias=true;
      final Offset circleOffset = Offset(configuration.size!.width/2 -radius/2,configuration.size!.height);

    canvas.drawCircle(offset+circleOffset, radius, _paint);
  }
}
