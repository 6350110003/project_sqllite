import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Search extends StatelessWidget {
  const Search({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Search'),
        backgroundColor: Colors.black,
        actions: [
          IconButton
            (onPressed:(){
              showSearch(
                context: context,
                delegate: CustomSearchDelegate(),);
          },
            icon: const Icon(Icons.search) ,
          )
        ],
      ),
      body:Container(
    width: double.maxFinite,
    height: double.maxFinite,

    decoration: BoxDecoration(
    image: DecorationImage(
    image: AssetImage(
    'asset/images/back_3.jpg'
    ),
    fit: BoxFit.cover
    )
    ),

          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Center(
                  child: Container(padding: EdgeInsets.symmetric(horizontal: 20,vertical: 5),
                    width: 100,
                    height: 65,
                    decoration: BoxDecoration(
                        color: Colors.white54,
                        borderRadius: BorderRadius.circular(29)),
                    child:  IconButton
                      (onPressed:(){
                      showSearch(
                        context: context,
                        delegate: CustomSearchDelegate(),);
                    },
                      icon: const Icon(Icons.search,size: 50,) ,

                    ),

                  ),
                ),
              ),
            ],
          )

      ),




    );
  }
}
class CustomSearchDelegate extends SearchDelegate {
  List<String> searchTerms =[
    'whit table',


  ];
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: (){
          query='';
        },
      ),
    ];
  }


  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon:  const  Icon(Icons.arrow_back) ,
        onPressed: (){
        close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    List<String> matchQuery =[];
    for(var cafe in searchTerms){
      if (cafe.toLowerCase().contains(query.toLowerCase())){
        matchQuery.add(cafe);
      }
    }
   return ListView.builder(
       itemCount: matchQuery.length,
     itemBuilder: (context,index){
         var result = matchQuery[index];
         return ListTile(
           title: Text(result),
         );
     },
   ) ;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<String> matchQuery =[];
    for(var cafe in searchTerms){
      if(cafe.toLowerCase().contains(query.toLowerCase())){
        matchQuery.add(cafe);
      }
    }
    return ListView.builder(
      itemCount: matchQuery.length,
      itemBuilder: (context,index){
        var result = matchQuery[index];
        return ListTile(
          title: Text(result),
        );
      },
    ) ;

  }


}
