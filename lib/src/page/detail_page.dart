import 'package:flutter/material.dart';
import 'package:project_sqllite/src/page/homepage.dart';
import 'package:project_sqllite/src/page/responsive_button.dart';
import 'package:project_sqllite/src/spllite/pages/add_profile.dart';
import 'package:project_sqllite/src/widgets/app_buttons.dart';
import 'package:project_sqllite/src/widgets/app_large_text.dart';
import 'package:project_sqllite/src/widgets/app_text.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {

  int gottenStars=4;
  int selectedIndex=-1;

  final images ={
    "asset/images/cake.jpg",
    "asset/images/drink.jpg",
    "asset/images/food.jpg",
    "asset/images/icecreem.jpg",
  };



  @override
  Widget build(BuildContext context) {


    return Scaffold(


      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Stack(
          children: [
            Positioned(
              left:0,
                right: 0,
                child: Container(
                  width: double.maxFinite,
                  height: 350,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image:AssetImage('asset/images/cake.jpg'),
                      fit: BoxFit.cover
                    ),
                  ),

            )),
            Positioned(
                left: 20,
                top: 50,
                child: Row(
                  children: [
                    IconButton(
                        icon:Icon(Icons.arrow_circle_left_outlined,size: 30,color: Colors.white,),
                        onPressed: () {
                          Navigator.pop(context,
                              MaterialPageRoute(builder: (context) {
                                return HomePage();
                              }));
                        })
                  ],
                )),
            Positioned(
              top: 320,
                child: Container(
                  padding: const EdgeInsets.only(left: 20,right: 20,top: 30),

                  width: MediaQuery.of(context).size.width,
                  height: 500,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30)
                    )
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          AppLargeText(text: 'Whit Table',color: Colors.black.withOpacity(0.8),),
                          AppLargeText(text: '\$ 250',color: Colors.brown,),

                        ],
                      ),
                      SizedBox(height: 10,),
                      Row(
                        children: [
                          Icon(Icons.location_on,color: Colors.brown),
                          SizedBox(width: 5,),
                          AppText(text: "Phuket,Thalang",color: Colors.brown,)
                        ],
                      ),
                      SizedBox(height: 20,),
                     Row(
                       children: [
                         Wrap(
                           children: List.generate(5, (index) {
                             return Icon(Icons.star,color:index<gottenStars?Colors.amber:Colors.grey,size: 20,);
                           }),
                         ),
                         SizedBox(width: 10,),
                         AppText(text: '(4.0)',color: Colors.black54,)
                       ],
                     ),
                      SizedBox(height: 25,),
                      AppLargeText(text: 'People',color: Colors.black.withOpacity(0.8),size: 20,),
                      SizedBox(height: 5,),
                      AppText(text: 'Number of people in your group',color: Colors.black38,),
                      SizedBox(height: 10,),
                      Wrap(
                        children: List.generate(5, (index){
                          return InkWell(
                            onTap: (){
                              setState((){
                                selectedIndex=index;
                              });

                            },
                            child: Container(
                              margin: const EdgeInsets.only(right: 10),
                              child: AppButtons(size: 50,
                                  color: selectedIndex==index?Colors.white:Colors.black,
                                  backgroundColor: selectedIndex==index?Colors.black:Colors.black12,
                                  borderColor: selectedIndex==index?Colors.black:Colors.black12,
                                  text: (index+1).toString(),


                              ),
                            ),
                          );
                        }),
                      ),
                      SizedBox(height: 20,),
                      AppLargeText(text: 'Description',color: Colors.black.withOpacity(0.8),size: 20),
                      SizedBox(height: 10,),
                      AppText(text: 'The toast and ice-creams are a must! Locals’ favorite and also my friends from all over the world love this place. Cosy atmosphere with reasonable prices.',color: Colors.black38,)
                    ],
                  ),


            )),
            Padding(
              padding: const EdgeInsets.only(top: 700,left: 30),
              child: SizedBox(
                width: 330,
                height: 70,
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.brown),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          )
                      )
                  ),
                  child: Text("Reserve your seat now",style: TextStyle(fontSize: 20),),
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return AddProfile();
                        })
                    );
                  },
                ),
              ),
            ),

          ],

        ),
      ),
    );
  }
}
