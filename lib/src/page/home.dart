import 'package:flutter/material.dart';
import 'package:project_sqllite/src/page/add_photo.dart';
import 'package:project_sqllite/src/page/homepage.dart';
import 'package:project_sqllite/src/page/search.dart';
import 'package:project_sqllite/src/page/user.dart';
import 'package:project_sqllite/src/spllite/sqlmain.dart';

class Home extends StatefulWidget {
  const Home({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List pages =[
    HomePage(),
    Search(),
    App(),
    LoginPage(title: '',)


  ];
  int currentIndex=0;
  void onTap(int index){
    setState((){
      currentIndex = index;
    });
  }

  @override

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body:pages[currentIndex],
      bottomNavigationBar:BottomNavigationBar(
        unselectedFontSize: 0,
        selectedFontSize: 0,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        onTap: onTap,
        currentIndex: currentIndex,
        selectedItemColor: Colors.pink,
        unselectedItemColor: Colors.grey.withOpacity(0.5),
        showUnselectedLabels: false,
        showSelectedLabels: false,
        elevation: 0,



        items: const[
          BottomNavigationBarItem( icon: Icon(Icons.photo),label:('homepage')),
          BottomNavigationBarItem( icon: Icon(Icons.search),label:('search')),
          BottomNavigationBarItem( icon: Icon(Icons.chair),label:('add')),
          BottomNavigationBarItem( icon: Icon(Icons.person_outline_rounded),label:('user')),






        ],),


    );
  }
}
